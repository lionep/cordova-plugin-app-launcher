//
//  AppLauncher.h
//  AppLauncher
//
//  Created by Lionel Penaud on 02/10/2014.
//
//

#ifndef App_launcher_h
#define App_launcher_h

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

@interface AppLauncher : CDVPlugin

- (void)launchApp:(CDVInvokedUrlCommand*)command;

@end

#endif

