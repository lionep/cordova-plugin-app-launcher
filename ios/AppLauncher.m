//
//  AppLauncher.m
//  AppLauncher
//
//  Created by Lionel Penaud on 02/10/2014.
//
//

#import "AppLauncher.h"
#import <Cordova/CDV.h>

@interface AppLauncher ()

@end

@implementation AppLauncher

- (void)launchApp:(CDVInvokedUrlCommand*)command
{
    NSString *bundleId = [[command arguments] objectAtIndex:0];
    NSLog(@"BundleId: %@", bundleId);
}

@end
